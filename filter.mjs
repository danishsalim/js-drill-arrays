function filter(elements, cb) {
  if (Array.isArray(elements)) {
    // first checking if elements is array or not
    let filtered = [];
    for (let element of elements) {
      // iterating over array and passing each element to callback function
      if (cb(element)) {
        //cb returns true pushing element to filtered array
        filtered.push(element);
      }
    }
    return filtered;
  } else {
    return;
  }
}

export default filter;
