function find(elements, cb) {
  if (Array.isArray(elements)) {
    for (let element of elements) {
      // first checking if elements is array or not
      if (cb(element)) {
        // pasiing each element to cb
        return element;
      }
    }
    //if element not found then returning undefined
    return undefined;
  } else {
    return;
  }
}

export default find;
