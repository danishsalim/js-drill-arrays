function reduce(elements, cb, startingValue) {
  if (Array.isArray(elements)) {
    // first checking if elements is array or not
    let accumulator = 0;
    if (startingValue) {
      accumulator = startingValue;
    }
    for (let element of elements) {
      //updating current
      accumulator = cb(accumulator, element);
    }
    return accumulator;
  }
}

export default reduce;
