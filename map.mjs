function map(elements, cb) {
  if (Array.isArray(elements)) {
    // first checking if elements is array or not
    let mappedArray = [];
    for (let index = 0; index < elements.length; index++) {
      // iterating over array and passing each element to callback function
      mappedArray.push(cb(elements[index], index));
    }
    return mappedArray;
  } else {
    return;
  }
}

export default map;
