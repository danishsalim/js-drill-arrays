import find from "../find.mjs";
import items from "../items.mjs";

const result = find(items, (item) => {
  if (item > 3) {
    return true;
  }
});

console.log(result)