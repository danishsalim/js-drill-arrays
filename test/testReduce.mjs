import items from "../items.mjs";
import reduce from "../reduce.mjs";

const cb = (accumulator, current) => {
  return accumulator + current;
};
const result = reduce(items, cb, 0);

console.log(result);
