function each(elements, cb) {
  if (Array.isArray(elements)) {
    // first checking if elements is array or not
    for (let index = 0; index < elements.length; index++) {
      // iterating over array and passing each element to callback function
      cb(elements[index], index);
    }
  } else {
    return;
  }
}

export default each;
