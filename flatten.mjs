let arr = [];
function flatten(elements) {
  if (Array.isArray(elements)) {
    // first checking if elements is array or not
    for (let element of elements) {
      if (!Array.isArray(element)) {
        arr.push(element);
      }
      //recursively calling function
      flatten(element);
    }
    return arr;
  } else {
    return;
  }
}

export default flatten;
